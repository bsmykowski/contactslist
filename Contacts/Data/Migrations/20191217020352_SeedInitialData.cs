﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Contacts.Data.Migrations
{
    public partial class SeedInitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Contacts",
                columns: new[] { "Id", "Category", "DateOfBirth", "Email", "FirstName", "LastName", "PhoneNumber", "SubCategory" },
                values: new object[] { 1, "Służbowy", new DateTime(2019, 12, 17, 3, 3, 51, 699, DateTimeKind.Local).AddTicks(9016), "kslowacki@pl.pl", "Karol", "Słowacki", "123123123", "subcategory" });

            migrationBuilder.InsertData(
                table: "Contacts",
                columns: new[] { "Id", "Category", "DateOfBirth", "Email", "FirstName", "LastName", "PhoneNumber", "SubCategory" },
                values: new object[] { 2, "Służbowy", new DateTime(2019, 12, 18, 3, 3, 51, 704, DateTimeKind.Local).AddTicks(71), "pkwiatek@com.com", "Paweł", "Kwiatkowski", "321321321", "subcategory" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
