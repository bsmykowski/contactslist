﻿using Contacts.Models;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contacts.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Subcategory> SubCategories { get; set; }

        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var rng = new Random();

            var categories = new []
            {
                new { Id = 1, CategoryName = "Służbowy" }, 
                new { Id = 2, CategoryName = "Prywatny" }, 
                new { Id = 3, CategoryName = "Inny" }
            };

            var subcategories = new[]
            {
                new { Id = 1, SubcategoryName = "Szef", CategoryId = 1 },
                new { Id = 2, SubcategoryName = "Klient", CategoryId = 1 },
                new { Id = 3, SubcategoryName = "Kolega z zespołu", CategoryId = 1 },
                new { Id = 4, SubcategoryName = "Mechanik", CategoryId = 3  }
            };

            modelBuilder.Entity<Category>().HasData(categories);

            modelBuilder.Entity<Subcategory>().HasData(subcategories);

            modelBuilder.Entity<Contact>().HasData(
            new
            {
                Id = 1,
                FirstName = "Karol",
                LastName = "Słowacki",
                Email = "kslowacki@pl.pl",
                CategoryId = 1,
                SubcategoryId = 1,
                PhoneNumber = "123123123",
                DateOfBirth = DateTime.Parse("1993-04-05")
            },
            new
            {
                Id = 2,
                FirstName = "Paweł",
                LastName = "Kwiatkowski",
                Email = "pkwiatek@com.com",
                CategoryId = 1,
                SubcategoryId = 2,
                PhoneNumber = "321321321",
                DateOfBirth = DateTime.Parse("1995-06-12")
            });
        }
    }
}
