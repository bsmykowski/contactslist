﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contacts.Models
{
    public class Subcategory
    {
        [Key]
        public int Id { get; set; }

        public string SubcategoryName { get; set; }

        public Category Category { get; set; }

        public int CategoryId { get; set; }

    }
}
