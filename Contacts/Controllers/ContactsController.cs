﻿using Contacts.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contacts.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContactsController : ControllerBase
    {

        private readonly ApplicationDbContext _context;

        public ContactsController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Contact> Get()
        {
            return _context
                .Contacts
                .Include(c => c.Category)
                .Include(c => c.Subcategory)
                .ToList();
        }

        [HttpGet]
        [Route("{contactId}")]
        public Contact Get(int contactId)
        {
            return _context.Contacts
              .Include(c => c.Category)
              .Include(c => c.Subcategory)
              .SingleOrDefault(x => x.Id == contactId);
        }

        [Authorize]
        [HttpDelete]
        [Route("{contactId}")]
        public void Delete(int contactId)
        {
            var contact = _context.Contacts.Find(contactId);
            _context.Contacts.Remove(contact);
            _context.SaveChanges();
        }

        [Authorize]
        [HttpPost]
        [Route("create")]
        public void Create(Contact contact)
        {
            _context.Contacts.Add(contact);
            _context.SaveChanges();
        }

        [Authorize]
        [HttpPut]
        [Route("update")]
        public void Update(Contact contact)
        {
            _context.Contacts.Update(contact);
            _context.SaveChanges();
        }
    }
}
