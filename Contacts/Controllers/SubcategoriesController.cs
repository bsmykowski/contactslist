﻿using Contacts.Data;
using Contacts.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contacts.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SubcategoriesController : ControllerBase
    {

        private readonly ApplicationDbContext _context;

        public SubcategoriesController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Subcategory> Get()
        {
            return _context
                .SubCategories
                .Include(sc => sc.Category)
                .ToList();
        }

        [Authorize]
        [HttpPost]
        [Route("create")]
        public void Create(Subcategory subcategory)
        {
            _context.SubCategories.Add(subcategory);
            _context.SaveChanges();
        }

    }
}
